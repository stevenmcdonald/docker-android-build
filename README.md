# Docker Android Build

Generic build env for Android apps

## Getting started

* on x86: `docker run --rm -v /home/scm/code/foo:/build/foo -it stevenmcdonald/android-build-generic`
* on arm: `docker run --platform linux/amd64 --rm -v /home/scm/code/foo:/build/foo -it stevenmcdonald/android-build-generic`

## Build:

* on x86: `docker build -t stevenmcdonald/android-build-generic .`
* on arm: `docker buildx build --platform linux/amd64 -t stevenmcdonald/android-build-generic .`